from flask import Flask
import folium
import database
import pandas as pd
import json

app = Flask(__name__)

@app.route('/')
def index():

    # on charge la base de données centrée sur Grenoble
    m = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13.5)
    folium.raster_layers.TileLayer('Open Street Map').add_to(m)
    folium.raster_layers.TileLayer('Stamen Terrain').add_to(m)
    folium.raster_layers.TileLayer('Stamen Toner').add_to(m)
    folium.raster_layers.TileLayer('Stamen Watercolor').add_to(m)
    folium.raster_layers.TileLayer('CartoDB Positron').add_to(m)
    folium.raster_layers.TileLayer('CartoDB Dark_Matter').add_to(m)

    # On crée la connexion à  la base de données
    conn = database.create_connection()

    # On charge les points d'intérêts depuis la base de données
    cur = database.query_create_select(conn, "select * from coord_points_interets;")

    # On crée la dataframe contenant les données
    # Element 6 et 7 du curseur: coordonnées du point

    latitude = []
    longitude = []
    texte_point_interet = []
    titre = []
    genre = []

    for ligne in cur:
        latitude.append(ligne[7])
        longitude.append(ligne[6])
        texte_point_interet.append("<a href=" + ligne[11] + "" " target=""_blank>" + ligne[11] + "</a>" + '\n' + ligne[8])
        titre.append(ligne[2])
        genre.append(ligne[14])


    # Maintenant on construit le dataframe pour la fonction folium.Marker

    data = pd.DataFrame({
        'lat': latitude,
        'lon': longitude,
        'titre': titre,
        'name': texte_point_interet,
        'genre': genre
    })

    # On crée le groupe "Points d'Intérêt"
    lgd_txt = '<span style="color: {col};">{txt}</span>'

    group01_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (fem.)", col="darkgreen"))
    group01_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (masc.)", col="green"))
    group02_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (fem.)", col="gray"))
    group02_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (masc.)", col="lightgray"))
    group03_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel(fem.)", col="red"))
    group03_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Patrimoine Naturel (masc.)", col="darkred"))
    group04_fem = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (fem.)", col="darkblue"))
    group04_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire et Evolution (masc.)", col="blue"))
    group05_masc = folium.FeatureGroup(name=lgd_txt.format(txt="Test Laurent (masc.)", col="blue"))

    # Maintenant on associe les points d'intérêt à la carte
    for i in range(0, len(data)):
        # print(data)
        # On colore differemment les points d'intérets s'ils sont à consonnance masculine ou  féminine
        if (data.iloc[i]['genre'] == "masculin"):  # COULEUR A CONSONNANCE NOM MASCULIN
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200], icon=folium.Icon(color='blue', icon='info-sign')).add_to(group01_masc)
        elif (data.iloc[i]['genre'] == "feminin"):  # COULEUR A CONSONNANCE NOM FEMININ
            folium.Marker([data.iloc[i]['lon'], data.iloc[i]['lat']], popup=data.iloc[i]['name'][0:200], icon=folium.Icon(color='red', icon='info-sign')).add_to(group01_fem)

    # On ajoute les groupes à la carte
    group01_fem.add_to(m)
    group01_masc.add_to(m)
    group02_fem.add_to(m)
    group02_masc.add_to(m)
    group03_fem.add_to(m)
    group03_masc.add_to(m)
    group04_fem.add_to(m)
    group04_masc.add_to(m)
    group05_masc.add_to(m)

    # group1 = folium.FeatureGroup(name='<span style=\\"color: red;\\">Routes Féminines</span>')              # (, show=False) a ajouter pour que la checkbox soit décochée au départ
    group1 = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Féminines", col="red"))

    # On va chercher les données ne base concernant les rues dont le nom est féminin
    cur = database.query_create_select(conn, "select genre, geojson, voie_complet From nom_des_voies where geojson not like '%[[[%' and genre = 'feminin';")

    for ligne in cur:
        geojson = ligne[1]
        print(ligne[2])
        tooltiptext = ligne[2]

        couleur = "red" # On affiche les rues dont le nom est féminin
        data = json.loads(geojson)

        if data['type'] == 'LineString':
            points = []

            for coord in data['coordinates']:
                points.append((coord[1], coord[0]))

                folium.PolyLine(points, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group1)

        if data['type'] == 'MultiLineString':
            for line in data['coordinates']:
                points = []
                for coord in line:
                    points.append((coord[1], coord[0]))

                folium.PolyLine(points, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group1)

    group1.add_to(m)

    # group2 = folium.FeatureGroup(name='<span style=\\"color: blue;\\">Routes Masculines</span>')
    group2 = folium.FeatureGroup(name=lgd_txt.format(txt="Routes Masculines", col="blue"))


    # On va maintenant chercher les données en base concernant les rues dont le nom est masculin
    cur = database.query_create_select(conn, "select genre, geojson, voie_complet From nom_des_voies where geojson not like '%[[[%' and genre = 'masculin';")

    for ligne in cur:
    #    print(ligne)
        geojson = ligne[1]
        tooltiptext = ligne[2]

        couleur = "blue" # On affiche les rues dont le nom est masculin
        data = json.loads(geojson)

        if data['type'] == 'LineString':

            points = []

            for coord in data['coordinates']:
                points.append((coord[1], coord[0]))

                folium.PolyLine(points, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group2)

        if data['type'] == 'MultiLineString':
            for line in data['coordinates']:
                points = []
                for coord in line:
                    points.append((coord[1], coord[0]))

                folium.PolyLine(points, color=couleur, weight=5, opacity=0.6, popup=tooltiptext).add_to(group2)

    group2.add_to(m)
    folium.LayerControl().add_to(m)
    m.save("templates/street.html")
    return m._repr_html_()

if __name__ == '__main__':
    app.run(port=5000)
#    app.run(debug=True)